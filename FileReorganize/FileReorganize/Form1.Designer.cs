﻿
namespace FileReorganize
{
    partial class FileReorganize
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.btnBrowser = new System.Windows.Forms.Button();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.rtbError = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFolderPrefix = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Folder";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(198, 49);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.ReadOnly = true;
            this.txtFolder.Size = new System.Drawing.Size(782, 35);
            this.txtFolder.TabIndex = 1;
            // 
            // btnBrowser
            // 
            this.btnBrowser.Location = new System.Drawing.Point(1040, 49);
            this.btnBrowser.Name = "btnBrowser";
            this.btnBrowser.Size = new System.Drawing.Size(162, 40);
            this.btnBrowser.TabIndex = 2;
            this.btnBrowser.Text = "Browser Folder";
            this.btnBrowser.UseVisualStyleBackColor = true;
            this.btnBrowser.Click += new System.EventHandler(this.btnBrowser_Click);
            // 
            // rtbLog
            // 
            this.rtbLog.Location = new System.Drawing.Point(13, 198);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.Size = new System.Drawing.Size(569, 760);
            this.rtbLog.TabIndex = 3;
            this.rtbLog.Text = "";
            // 
            // rtbError
            // 
            this.rtbError.ForeColor = System.Drawing.Color.Red;
            this.rtbError.Location = new System.Drawing.Point(633, 198);
            this.rtbError.Name = "rtbError";
            this.rtbError.Size = new System.Drawing.Size(569, 760);
            this.rtbError.TabIndex = 4;
            this.rtbError.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 30);
            this.label2.TabIndex = 5;
            this.label2.Text = "Folder Name Prefix";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtFolderPrefix
            // 
            this.txtFolderPrefix.Location = new System.Drawing.Point(248, 114);
            this.txtFolderPrefix.Name = "txtFolderPrefix";
            this.txtFolderPrefix.Size = new System.Drawing.Size(732, 35);
            this.txtFolderPrefix.TabIndex = 6;
            this.txtFolderPrefix.Text = "Folder";
            // 
            // FileReorganize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1246, 988);
            this.Controls.Add(this.txtFolderPrefix);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rtbError);
            this.Controls.Add(this.rtbLog);
            this.Controls.Add(this.btnBrowser);
            this.Controls.Add(this.txtFolder);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1270, 1052);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1270, 1052);
            this.Name = "FileReorganize";
            this.Text = "File Reorganize";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnBrowser;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.RichTextBox rtbError;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFolderPrefix;
    }
}

