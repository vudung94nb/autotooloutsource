﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileReorganize
{
    public partial class FileReorganize : Form
    {

        public FileReorganize()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBrowser_Click(object sender, EventArgs e)
        {
            rtbLog.Text = "";
            rtbError.Text = "";
            Regex rx = new Regex(@"^([\d]*).*$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            var lstFolder = new List<string>();
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            if(folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                var folderPath = folderBrowserDialog.SelectedPath;
                txtFolder.Text = folderPath;

                var folderContent = Directory.GetFiles(folderPath);
                if(folderContent.Length == 0)
                {
                    rtbLog.Text = "This folder is empty\n";
                    return;
                }

                foreach(String file in folderContent)
                {
                    var fileName = Path.GetFileName(file);
                    var matchResult = rx.Matches(fileName)[0].Groups[1].Value.Trim();
                    if (!string.IsNullOrWhiteSpace(matchResult))
                    {
                        var folderName = string.Format("{0} {1}", txtFolderPrefix.Text, matchResult);
                        folderName = Path.Combine(folderPath, folderName);

                        if (!lstFolder.Contains(folderName))
                        {
                            rtbLog.Text += "Creating folder " + folderName + "\n";
                            Directory.CreateDirectory(folderName);
                            lstFolder.Add(folderName);
                        }

                        rtbLog.Text += string.Format("Moving file {0} to {1}\n" , file, folderName);
                        try
                        {
                            File.Move(file, Path.Combine(folderName, fileName));
                        }
                        catch (Exception ex)
                        {
                            rtbError.Text += string.Format("Cannot move file with error={0}\n", ex.Message);
                        }
                    }
                    else
                    {
                        rtbError.Text += string.Format("The file {0} does not contain number at the begining! This file will be ignored\n", file);
                    }
                }
            }
        }
    }
}
